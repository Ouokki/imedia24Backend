# iMedia24 Coding challenge

# Endpoints
For further reference, please consider the following sections:

* GET "/products/{sku}" -> sku : string
* GET "/products"       -> parameter -> example : /products?skus=123,4567,8901,2345,67789
* POST "/AddProduct"     -> body : ProductEntity(skuProduct) 
* PUT "/UpdateProduct/{sku}" - > body : ProductEntity(skuProduct)


# Tests
These additional references should also help you:

* Tests  in "/src/test/kotlin/de/imedia24/shop/ShopApplicationTests.kt"
* Testing a POST and GET method

# Putting the application to Docker
These additional references should also help you:
## Steps :
* 1- ./gradlew build
* 2- Build the image using : docker build -t shop (use of Dockerfile)
* 3- Run the image using docker run -d -p 5000:5000 —name shop
                (-d : mode detached || -p : port )

# [this is just a test on top of k8s cluster] 
# only complex apps which has multiple modules should be deployed & managed in k8s cluster
## Steps :
* 1- Applying the deployment file kubectl apply -f deployment.yml 
* 2- kubectl get svc to get services
* 3- kubectl get pods to get the units ruuning 
### The app should be up .








