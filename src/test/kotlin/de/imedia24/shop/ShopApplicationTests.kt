package de.imedia24.shop

import de.imedia24.shop.db.entity.ProductEntity
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.context.annotation.Bean
import org.springframework.http.HttpEntity
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.math.BigDecimal
import java.net.URI
import java.time.ZonedDateTime

@ExtendWith(SpringExtension::class)
@SpringBootTest(
		webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
		classes = [ShopApplicationTests.ControllerTestConfig::class],
		properties = ["spring.example.property=foobar"]
)
@ActiveProfiles(value = ["test"])

class ShopApplicationTests {
	var testRestTemplate = TestRestTemplate()

	@LocalServerPort
	var serverPort: Int = 8080

	@TestConfiguration
	internal class ControllerTestConfig {

	}

	private fun applicationUrl() = "http://localhost:8080"
	@Test
	fun contextLoads() {
	}

	@Test
	fun simplePostTest() {
		val post=testRestTemplate.exchange(
				URI(applicationUrl() + "/AddProduct"),
						HttpMethod.POST,
				HttpEntity(
						ProductEntity("test",
								"Pokemon",
								"description",
								BigDecimal.valueOf(2.0),
								"Informations",
								ZonedDateTime.now(),
								ZonedDateTime.now()
								)
				),
				String::class.java
		)
		Assertions.assertEquals(HttpStatus.OK, post.statusCode)
		/*val result = testRestTemplate.exchange(
				URI(applicationUrl() + "/products/1"),
				HttpMethod.GET,
				HttpEntity(""),
				String::class.java)

		Assertions.assertEquals(HttpStatus.OK, result.statusCode)
		Assertions.assertEquals("world", result.body)*/
	}

	@Test
	fun simpleGetTest() {
		val result = testRestTemplate.exchange(
				URI(applicationUrl() + "/products/test"),
				HttpMethod.GET,
				HttpEntity(""),
				String::class.java)

		Assertions.assertEquals(HttpStatus.OK, result.statusCode)

	}

}
