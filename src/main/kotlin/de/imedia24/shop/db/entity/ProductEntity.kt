package de.imedia24.shop.db.entity

import org.hibernate.annotations.UpdateTimestamp
import java.math.BigDecimal
import java.time.ZonedDateTime
import javax.persistence.*

@Entity
@Table(name = "products")
data class ProductEntity(
    @Id
    @Column(name = "sku", nullable = false)
    val sku: String,

    @Column(name = "name", nullable = false)
    var name: String,

    @Column(name = "description")
    var description: String? = null,

    @Column(name = "price", nullable = true)
    var price: BigDecimal,

    @Column(name = "stock_informations", nullable = false)
    var stock_informations: String?=null,

    @UpdateTimestamp
    @Column(name = "created_at", nullable = true)
    var createdAt: ZonedDateTime,

    @UpdateTimestamp
    @Column(name = "updated_at", nullable = true)
    var updatedAt: ZonedDateTime
) {
    constructor() : this("", "","",BigDecimal.valueOf(0.0),"",ZonedDateTime.now(),ZonedDateTime.now())
}
