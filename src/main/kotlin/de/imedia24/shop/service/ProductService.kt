package de.imedia24.shop.service

import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.db.repository.ProductRepository
import de.imedia24.shop.domain.product.ProductRequest
import de.imedia24.shop.domain.product.ProductRequest.Companion.toProductEntity
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.domain.product.ProductResponse.Companion.toProductResponse
import javassist.NotFoundException


import org.springframework.stereotype.Service
import java.math.BigDecimal
import java.time.ZonedDateTime
import kotlin.collections.ArrayList


@Service
class ProductService(private val productRepository: ProductRepository) {

    fun findProductBySku(sku: String): ProductResponse? {
        val skuProduct = productRepository.findBySku(sku)
        if (skuProduct == null) {
            throw NotFoundException("Not Found")
        }else {
            return skuProduct.toProductResponse()

        }
    }

    fun findProductBySkuIn(skus: String): List<ProductResponse>? {
        val delim = ","
        val list = skus.split(delim)
        val skusProduct = productRepository.findBySkuIn(list as ArrayList<String>)
        if (skusProduct == null) {
            throw NotFoundException("Not Found")
        }else {
            return skusProduct.map { skuProduct -> skuProduct.toProductResponse() }
        }
    }

     fun addProductSku(sku: ProductRequest): ProductResponse {
        return productRepository.save(sku.toProductEntity()).toProductResponse()
    }

    fun updateProductSku(sku: String, skuRequest: ProductRequest): ProductResponse {
        val skuProduct = productRepository.findBySku(sku)
        println(skuRequest.name)
        if (skuProduct!=null){
            println("test")
            skuProduct.apply {
                skuProduct.name = skuRequest.name;
                skuProduct.description=skuRequest.description;
                skuProduct.price=skuRequest.price;
                skuProduct.stock_informations=skuRequest.stock_informations;
                skuProduct.createdAt=skuProduct!!.createdAt;
                skuProduct.updatedAt=ZonedDateTime.now();
            }
            productRepository.save(skuProduct!!)
        }else {
            throw NotFoundException("Cannot update")
        }
        return skuProduct.toProductResponse()
    }


}
