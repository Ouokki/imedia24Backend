package de.imedia24.shop.controller

import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.domain.product.ProductRequest
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.service.ProductService
import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.websocket.server.PathParam

@RestController
class ProductController(private val productService: ProductService) {

    private val logger = LoggerFactory.getLogger(ProductController::class.java)!!

    @GetMapping("/products/{sku}", produces = ["application/json;charset=utf-8"])
    fun findProductsBySku(
        @PathVariable("sku") sku: String
    ): ResponseEntity<ProductResponse> {
        logger.info("Request for product $sku")

        val product = productService.findProductBySku(sku)
        return if(product == null) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(product)
        }
    }
    @GetMapping("/products", produces = ["application/json;charset=utf-8"])
    fun findProductsBySkuIn(
            @RequestParam(value = "skus", defaultValue = "null") skus : String
    ): ResponseEntity<List<ProductResponse>> {
        logger.info("Request for product $skus")
        //Returns An array
        val products = productService.findProductBySkuIn(skus)
        return if(products == null) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(products)
        }
    }


    @PostMapping(
            value = ["/AddProduct"],
            produces = ["application/json"],
            consumes = ["application/json"]
    )
    fun createProduct(@RequestBody body: ProductRequest): ResponseEntity<ProductResponse> {
        val productResponse = productService.addProductSku(body)
        return if(productResponse == null) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(productResponse)
        }
    }
    @PutMapping(
            value = ["/UpdateProduct/{sku}"],
            produces = ["application/json"],
            consumes = ["application/json"]
    )
    fun updateProduct(@PathVariable("sku") id: String,
                      @RequestBody body: ProductRequest): ResponseEntity<ProductResponse> {
        val productResponse = productService.updateProductSku(id,body)
        return if(productResponse == null) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(productResponse)
        }
    }


}
